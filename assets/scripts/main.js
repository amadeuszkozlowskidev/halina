$(function() {

    $('#menu-pointer').click(function() {
        $('html, body').animate({
            scrollTop: $(".page2").offset().top - (window.innerHeight * 0.1)   }, 1000);
    });


    document.getElementById('hamburger-nav').onclick = function(){
        document.getElementById('main-navigation').classList.add("transitioned");
        if(document.getElementById('back-button')) document.getElementById('back-button').classList.add("transitioned");

        this.classList.add("transitioned");
    }

    document.getElementById('close-menu').onclick = function(){
        document.getElementById('main-navigation').classList.remove("transitioned");
        document.getElementById('hamburger-nav').classList.remove("transitioned");
        if(document.getElementById('back-button')) document.getElementById('back-button').classList.remove("transitioned");
    }
});